<?php
use Brain\Cortex\Route\RouteCollectionInterface;
use Brain\Cortex\Route\QueryRoute;
/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Serasa_Vitrine_Da
 * @subpackage Serasa_Vitrine_Da/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Serasa_Vitrine_Da
 * @subpackage Serasa_Vitrine_Da/public
 * @author     Equipe Tecnobiz <desenv@tecnobiz.com.br>
 */

class Serasa_Vitrine_Da_Public {
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Tamanho padrão da página de soluções
	 */
	const PAGE_SIZE = 4;

	/*
	* Constante para definir a url customizada
	*/
	const Rota = '/decisao/blog/';

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Cria pages templates do plugin vitrine serasa
	 *
	 * @since    1.0.0
	 */
	public function createPageTemplate($page_template)
	{
		//Verifica slug da url da página
		if(is_page( 'decisao' )){
			$page_template = dirname(__FILE__) . '/partials/pagina-inicial.php';
		}

		//Retorna a página a ser criada
		return $page_template;
	}

	/**
	 * Cria as singles dos Custom Post Types Soluções DA
	 *
	 * @since    1.0.0
	 */
	public function createCustomPostSingles($single_template)
	{
		global $post;

		if ($post->post_type === 'serasa-vitrine-da') {
			$single_template = dirname(__FILE__) . '/partials/single-vitrine.php';
		}

		return $single_template;
	}

	/**
	 * Cria a archive dos Custom Post Types Blog DA
	 *
	 * @since    1.0.0
	 */
	public function createCustomPostArchive($archive_template)
	{
		global $post;

		if ($post->post_type === 'blog-serasa-da') {
			$archive_template = dirname(__FILE__) . '/partials/archive-blog-serasa-da.php';
		}

		return $archive_template;
	}

	/**
	 * Cria as singles dos Custom Post Types Blog DA
	 *
	 * @since    1.0.0
	 */
	public function createCustomPostSinglesBlogDA($single_template)
	{
		global $post;

		if ($post->post_type === 'blog-serasa-da') {
			$single_template = dirname(__FILE__) . '/partials/single-blog-serasa-da.php';
		}

		return $single_template;
	}

	/**
	 * Retorna todos os tipos de solução e seus campos acf
	 * @param $taxonomy string tipo de taxonomia a ser filtrada
	 * @return array Array associativo com os dados dos tipos de soluções carregados.
	 */
	public static function get_information_of_public_taxonomy($taxonomy)
	{
		$type_of_public = get_terms(array(
			'taxonomy' => $taxonomy,
			'hide_empty' => false,
		));

		//Cria novo array
		$type_of_public_detail = array();

		//Percorre o array (objeto wp) retornado
		foreach ($type_of_public as $type_public) {

			//Agrupa informações do tipo de solução em um array
			$type_of_public_detail[$type_public->name] = array(
				'name' => $type_public->name,
				'description' =>$type_public->description,
				'slugTypeSolution' => $type_public->slug,
				'solucao_1' => (function_exists('get_field')) ?
					get_field('solucao_destaque_1', $taxonomy . '_' . $type_public->term_taxonomy_id) : '',
				'solucao_2' => (function_exists('get_field')) ?
					get_field('solucao_destaque_2', $taxonomy . '_' . $type_public->term_taxonomy_id) : '',
				'solucao_3' => (function_exists('get_field')) ?
					get_field('solucao_destaque_3', $taxonomy . '_' . $type_public->term_taxonomy_id) : '',
				'solucao_4' => (function_exists('get_field')) ?
					get_field('solucao_destaque_4', $taxonomy . '_' . $type_public->term_taxonomy_id) : '',
				'image' => (function_exists('get_field')) ?
					get_field('taxonomy_image_solucao', $taxonomy . '_' . $type_public->term_taxonomy_id) : '',
				'video' => (function_exists('get_field')) ?
					get_field('video', $taxonomy . '_' . $type_public->term_taxonomy_id) : '',
			);
		}

		//Retorna valor do array
		return $type_of_public_detail;
	}

	/**
	 * Retorna todos os posts do blog serasa DA
	 * diversos tipos de filtros (todos parâmetros opcionais).
	 * @param array $category_blog Tipo do curso (para crescer, para orientar, etc...).
	 * @param int $page_number Número da página à retornar.
	 * @param int $page_size Número de posts por página à retornar.
	 * @return array Array associativo com os dados dos posts encontrados.
	 */
	public static function get_posts_blog($category_blog = array(), $page_number = 1, $page_size = self::PAGE_SIZE)
	{
		$args = array(
			'post_type' => 'blog-serasa-da',
			'post_status' => 'publish',
			'tax-query' => array(),
			'posts_per_page' => $page_size,
			'paged' => $page_number,
			'page' => $page_number,
		);

		// Filtra solução por um ou mais tema.
		$tax_query_solution = array(
			'relation' => 'OR',
		);

		foreach ($category_blog as $type_solution) {
			$tax_query_solution[] = array(
				'taxonomy' => 'tipo-de-post-da',
				'field' => 'slug',
				'terms' => $type_solution,
			);
		}

		// Filtra soluções por solução escolhida.
		$args['tax_query'][] = $tax_query_solution;

		//Realiza a consulta e retorna a quantidade posts achados
		$posts_query = new WP_Query($args);

		//Quantidade de posts achados
		$total_posts_number = $posts_query->found_posts;

		//Conta para ver quantidade de páginas a retornar
		$result_total_pages = $total_posts_number / $page_size;

		if((is_float($result_total_pages))){
			$result_total_pages = ceil($result_total_pages);
		}

		//Cria array que receberá os objetos (posts)
		$posts = array();

		//Procura qual custom post type 'produtos' tem título semelhante ao $keyword
		foreach ($posts_query->posts as $singlePost){
			$posts[] = $singlePost;
		}

		$posts['total_pages'] = $result_total_pages;
		$posts['wp_query'] = $posts_query;

		return $posts;
	}

	/**
	 * @param string $taxonomy Taxonomia que será usada para filtrar posts
	 * @param string $term Termo que será usado para filtrar os posts
	 * @return string
	 */
	public static function get_posts_by_term($taxonomy, $term)
	{
		$args = array(
			'post_type' => 'serasa-vitrine-da',
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field' => 'slug',
					'terms' => $term,
				)
			)
		);

		$posts = get_posts($args);

		return $posts;
	}

	/**
	 * Retorna termos existentes nas taxonomias
	 * @param string $taxonomy Taxonomia que será usada para filtrar posts
	 * @return string
	 */
	public static function get_terms_blog($taxonomy) {
		$terms_blog = get_terms(array(
			'taxonomy' => $taxonomy,
			'hide_empty' => false,
		));

		return $terms_blog;
	}

	/**
	 * Cria páginação do blog
	 * @param int $total_pages Número total de páginas
	 * @param int $current_page Número da página atual
	 */
	public static function wpbeginner_numeric_posts_nav($total_pages, $current_page) {
		if ($total_pages > 1){
			if($current_page > 1){

			}else{
				$current_page = max(1, get_query_var('paged'));
			}

			echo paginate_links(array(
				'base' => get_pagenum_link(1) . '%_%',
				'format' => '/page/%#%',
				'current' => $current_page,
				'total' => $total_pages,
			));
		}
	}

	/**
	 * Contabiliza a quantidade de vezes que uma postagem é vista
	 * @param $postID
	 */
	public static function wpb_set_post_views($postID) {
		$count_key = 'wpb_post_views_count';
		$count = get_post_meta($postID, $count_key, true);

		if($count==''){
			$count = 0;
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
		}else{
			$count++;
			update_post_meta($postID, $count_key, $count);
		}

		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	}

	/**
	 * Facilita a manuntenção de contabilizar os posts
	 * @param $post_id
	 */
	public static function wpb_track_post_views ($post_id) {
		if ( !is_single() ) return;

		if ( empty ( $post_id) ) {
			global $post;
			$post_id = $post->ID;
		}

		Serasa_Vitrine_Da_Public::wpb_get_post_views($post_id);
	}

	/**
	 * @param $postID
	 * @return string
	 */
	public static function wpb_get_post_views($postID){
		$count_key = 'wpb_post_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
			return "0 View";
		}
		return $count.' Views';
	}

	/**
	 * Pega todos os posts mais visualizados
	 * @return array $newPopularPost
	 */
	public static function get_most_view_posts(){
		$args = array(
			'post_type' => 'blog-serasa-da',
			'meta_key' => 'wpb_post_views_count',
			'posts_per_page' => 4,
			'orderby' => 'meta_value_num',
			'order' => 'DESC',
		);

		$popularposts = new WP_Query( $args );

		$newPopularPost = array();

		foreach ($popularposts->posts as $popularpost){
			$newPopularPost[] = $popularpost;
		}

		return $newPopularPost;
	}

	/**
	 * Extrai os dados cadastrados de um repeater como array
	 * pronto à ser percorrido no loop do componente accordion-blue.
	 * Estrutura do repeater
	 * - <repeater-slug>
	 *   - <repeater-slug>-titulo
	 *   - <repeater-slug>-conteudo
	 *   - <repeater-slug>-conteudo-inicializa-aberto
	 * @see partials/accordion-blue.php
	 * @param $accordion_slug Slug do accordion à extrair os dados
	 * @return array Array formatapdo pronto para ser adicionado ao accordion
	 */
	public static function extract_accordion_array($accordion_slug)
	{
		if (function_exists('get_field') && '' !== $accordion_slug) {
			$audiencias = (get_field($accordion_slug) &&
			               is_array(get_field($accordion_slug))) ?
				get_field($accordion_slug) : array();

			$accordion_itens = array();
			foreach ($audiencias as $audiencia) {
				$accordion_itens[] = array(
					'title' => (array_key_exists($accordion_slug . '-titulo', $audiencia)) ?
						$audiencia[$accordion_slug . '-titulo'] : '',
					'content' => (array_key_exists($accordion_slug . '-conteudo', $audiencia)) ?
						$audiencia[$accordion_slug . '-conteudo'] : '',
					'open' => array_key_exists($accordion_slug . '-conteudo-inicializa-aberto', $audiencia) &&
					          $audiencia[$accordion_slug . '-conteudo-inicializa-aberto'],
				);
			}
			return $accordion_itens;
		}
	}

	/**
	 * Retorna dados sobre a url atual
	 * @return array $url_parts Retorna dados sobre a url
	 */
	public static function get_parts_url(){
		$url_parts = array();

		//Pega o link atual
		$url_parts['full_url'] = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		//Pega qual número da página: /page/2
		$url_parts['number_page'] = intval(basename($url_parts['full_url']));

		//Retorna os recursos da url
		$url = parse_url($url_parts['full_url']);
		$pices_url = explode("/",$url['path']);

		//Pega parte
		$category = strrpos($url_parts['full_url'], "categoria");
		if (!$category === false){
			$url_parts['category_exists'] = true;
			$url_parts['category_page'] = $pices_url[4];
		}else{
			$url_parts['category_exists'] = false;
		}

		$pos = strrpos($url_parts['full_url'], "page");
		if (!$pos === false){
			$url_parts['page_exists'] = true;
		}else{
			$url_parts['page_exists'] = false;
		}
		
		return $url_parts;
	}

	/**
	 * Muda caminho padrão seguido pelo WordPress para:
	 * Solucoes-Serasa/Solucao/Produto
	 * @param RouteCollectionInterface $routes
	 */
	public function routeUrlSolucoes(RouteCollectionInterface $routes) {

		$url_parts = Serasa_Vitrine_Da_Public::get_parts_url();
		$number = $url_parts['number_page'];
		$taxonomy = $url_parts['category_page'];

		$routes->addRoute(new QueryRoute(
			self::Rota,
			function(array $matches) {
				return [
					'post_type'   => 'blog-serasa-da',
				];
			}
		));

		$routes->addRoute(new QueryRoute(
			self::Rota .'page/'.$number,
			function(array $matches) {
				return [
					'post_type'   => 'blog-serasa-da',
				];
			}
		));

		$routes->addRoute(new QueryRoute(
			self::Rota.'categoria/'.$taxonomy .'/page/'.$number,
			function(array $matches) {
				return [
					'post_type'   => 'blog-serasa-da',
				];
			}
		));

		$routes->addRoute(new QueryRoute(
			self::Rota .'categoria/'.$taxonomy,
			function(array $matches) {
				return [
					'post_type'   => 'blog-serasa-da',
				];
			}
		));

		$routes->addRoute(new QueryRoute(
			self::Rota.'{postname}',
			function(array $matches) {
				return [
					'name'        => $matches['postname'],
					'post_type'   => 'blog-serasa-da',
					'post_status' => 'publish',
				];
			}
		));

	}

	/**
	 * Muda prev e next padrão seguido pelo WordPress para seguir a rota
	 */
	public function prev_and_next_posts() {

		$prev_post = get_previous_post();

		if($prev_post) {
			$urlPrev = (get_permalink($prev_post->ID));
			$urlPrevReplace = str_replace("blog-serasa-da", Serasa_Vitrine_Da_Public::Rota, $urlPrev );

			$prev_title = strip_tags(str_replace('"', '', $prev_post->post_title));

			echo "\t" . '<a rel="prev" href="' . $urlPrevReplace . '" title="' . $prev_title. '" class="prev-post default-prev-next">&laquo; Anterior<br /></a>' . "\n";
		}

		$next_post = get_next_post();
		if($next_post) {
			$urlNext = (get_permalink($next_post->ID));
			$urlNextReplace = str_replace("blog-serasa-da", Serasa_Vitrine_Da_Public::Rota, $urlNext );

			$next_title = strip_tags(str_replace('"', '', $next_post->post_title));

			echo "\t" . '<a rel="next" href="' . $urlNextReplace . '" title="' . $next_title. '" class="next-post default-prev-next">Próximo &raquo;<br /></a>' . "\n";
		}
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		global $post;

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Serasa_Vitrine_Da_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Serasa_Vitrine_Da_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if (is_page('decisao')){
			wp_enqueue_style( $this->plugin_name .'-header-interna', plugin_dir_url( __FILE__ ) . 'css/header-interna.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name .'-pagina-inicial', plugin_dir_url( __FILE__ ) . 'css/pagina-inicial.css', array(), $this->version, 'all' );
		}

		if (is_single()) {
			wp_enqueue_style( $this->plugin_name .'-breadcrumb-alternativo', plugin_dir_url( __FILE__ ) . 'css/breadcrumb.css', array(), $this->version, 'all' );
		}

		if($post->post_type === 'serasa-vitrine-da'){
			wp_enqueue_style( $this->plugin_name .'-header-interna', plugin_dir_url( __FILE__ ) . 'css/header-interna.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name . '-accordion', plugin_dir_url( __FILE__ ) . 'css/accordion-blue.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name . '-single', plugin_dir_url( __FILE__ ) . 'css/vitrine-single.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name . '-single-produto', plugin_dir_url( __FILE__ ) . 'css/vitrine-produtos.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name . '-gravity', plugin_dir_url( __FILE__ ) . 'css/gravity-css.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name . '-gravity-validation', plugin_dir_url( __FILE__ ) . 'css/gform-validation.css', array(), $this->version, 'all' );
		}

		if($post->post_type === 'blog-serasa-da') {
			wp_enqueue_style( $this->plugin_name .'-header-interna', plugin_dir_url(__FILE__) . 'css/header-interna.css',array(), $this->version, 'all');
			wp_enqueue_style( $this->plugin_name . '-breadcrumb', plugin_dir_url( __FILE__ ) . 'css/breadcrumb.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name . '-main-single', plugin_dir_url( __FILE__ ) . 'css/single-blog-serasa-da.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name . '-gravity-gravity', plugin_dir_url( __FILE__ ) . 'css/gravity-css.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name . '-gravity-validation', plugin_dir_url( __FILE__ ) . 'css/gform-validation.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name . '-sidebar-posts', plugin_dir_url( __FILE__ ) . 'css/sidebar.css', array(), $this->version, 'all' );
		}

		if( is_archive('blog-serasa-da') ) {
			wp_enqueue_style( $this->plugin_name .'-main-styles', plugin_dir_url(__FILE__) . 'css/archive-blog-serasa-da.css',array(), $this->version, 'all');
			wp_enqueue_style( $this->plugin_name .'-banner', plugin_dir_url(__FILE__) . 'css/banner-interna.css',array(), $this->version, 'all');
		}
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		global $post;

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Serasa_Vitrine_Da_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Serasa_Vitrine_Da_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if (is_page('decisao')) {
			wp_enqueue_script($this->plugin_name .'-menu', plugin_dir_url(__FILE__) . 'js/load-menu.js', array('jquery'), $this->version, false);
			wp_enqueue_script($this->plugin_name .'-select', plugin_dir_url(__FILE__) . 'js/load-select.js', array('jquery'), $this->version, false);
		}

		if($post->post_type === 'serasa-vitrine-da'){
			wp_enqueue_script($this->plugin_name .'-menu', plugin_dir_url(__FILE__) . 'js/load-menu.js', array('jquery'), $this->version, false);
			wp_enqueue_script($this->plugin_name .'-gravity', plugin_dir_url(__FILE__) . 'js/vitrine-gform.js', array('jquery'), $this->version, false);
		}

		if($post->post_type === 'blog-serasa-da') {
			wp_enqueue_script($this->plugin_name .'-menu', plugin_dir_url(__FILE__) . 'js/load-menu.js', array('jquery'), $this->version, false);
			wp_enqueue_script($this->plugin_name .'-gravity', plugin_dir_url(__FILE__) . 'js/vitrine-gform.js', array('jquery'), $this->version, false);
			wp_enqueue_script($this->plugin_name .'-facebook-comments', plugin_dir_url(__FILE__) . 'js/load-facebook-comments.js', array('jquery'), $this->version, false);
		}
	}



}
