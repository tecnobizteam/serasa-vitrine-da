<?php
wp_footer();

$template_url 	= get_bloginfo('template_url');
$site_url		= get_site_url();

$info = parse_url($site_url);
$host = str_replace('www.', '', $info['host']);
?>
<div class="bg_fff overflow col-xs-12 footer">
	<div class="container margin_t_35 overflow">
		<div class="col-xs-12 copyright visible-xs">
			©2017 Experian Information Solutions, Inc. Experian Marketing Services All rights reserved.
		</div>
		<div class="col-xs-12 visible-xs">
			<ul>
				<li class="margin_b_15"><a href="/comunicado-importante">Comunicado Importante</a></li>
				<li class="margin_b_15"><a href="/politica-de-privacidade">Política de Privacidade</a></li>
			</ul>
		</div>
		<div class="col-md-9">
			<?php $count_final_row = 0; $count = 0; $tipos_solucao = Serasa_Vitrine_Da_Public::get_information_of_public_taxonomy('tipo-de-solucao-da');
			foreach ($tipos_solucao as $tipo_solucao):?>
				<?php  if($count === 0):?>
					<div class="row">
				<?php endif; $count++; $count_final_row++; ?>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="footer-margin-title"><?php echo $tipo_solucao['name'];?></div>
							<ul>
								<?php  $posts = Serasa_Vitrine_Da_Public::get_posts_by_term('tipo-de-solucao-da', $tipo_solucao['slugTypeSolution']);
								foreach ($posts as $post):?>
									<li class="margin_b_10"><a class="footer-link-category" href="<?php echo get_permalink($post->ID)?>"><?php echo $post->post_title?></a></li>
								<?php endforeach; ?>
							</ul>
						</div>
                        <?php if($count_final_row === 5): ?>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                            	<a class="footer-margin-title blue-text-link" target="_blank" href="<?php echo $site_url .'/academia/'?>">Academia Serasa Experian</a>
                            </div>
                        <?php endif; ?>
						<?php if($count === 3): ?>
					</div>
				<?php $count = 0;  endif;?>
			<?php endforeach; ?>
        </div>
		</div>
        <div class="col-md-3">
            <div class="col-xs-12 col-sm-6 col-md-12">
                <div class="newsletter_footer">
                    <div >Newsletter</div>
					<?php echo do_shortcode("[gravityform id='Serasa Vitrine DA - Newsletter' title=false description=false ajax=true tabindex=7]");
					?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-12">
                <div class='social-footer'>
                    <div>Social</div>
                    <a href="https://www.facebook.com/serasaexperian/" target="_blank"><span class="margin_r_15 blog-sidebar-social-icon icon-facebook-inverse"></span></a>
                    <a href="https://twitter.com/serasaexperian?lang=pt" class="margin_r_15" target="_blank"><span class="blog-sidebar-social-icon icon-twitter-inverse"></span></a>
                    <a href="https://br.linkedin.com/company/serasa-experian" target="_blank"><span class="blog-sidebar-social-icon icon-linkedin-inverse margin_r_15"></span></a>
                    <a href="https://www.youtube.com/user/serasaexperian" target="_blank"><span class="blog-sidebar-social-icon icon-youtube-inverse "></span></a>
                </div>
            </div>
            <div class="hidden-xs hidden-sm" style="margin-top: 180px;">
                <!--- DO NOT EDIT - GlobalSign SSL Site Seal Code - DO NOT EDIT ---><table style="margin-top: 28px; display: inline-block; margin-left: 10px;" width=125 border=0 cellspacing=0 cellpadding=0 title="CLICK TO VERIFY: This site uses a GlobalSign SSL Certificate to secure your personal information." ><tr><td><span id="ss_img_wrapper_gmogs_image_125-50_en_dblue"><a href="https://www.globalsign.com/" target=_blank title="GlobalSign Site Seal" rel="nofollow"><img alt="SSL" border=0 id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_125-50_en.gif"></a></span><script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_125-50_en_dblue.js"></script></td></tr></table><!--- DO NOT EDIT - GlobalSign SSL Site Seal Code - DO NOT EDIT --->
            </div>
        </div>
	</div>
</div>
<div class="col-xs-12 box_end_f hidden-xs">
	<ul class="container f16 align_c">
		<li class="inline_block padding_r_15 padding_l_15"><a href="/comunicado-importante">Comunicado Importante</a></li>
		<li class="inline_block padding_r_15 padding_l_15"><a href="/politica-de-privacidade">Política de Privacidade</a></li>
	</ul>
	<div class="container">
		<div class="col-xs-12 col-md-6">
			<b>Serasa Experian - São Paulo</b><br>
			Alameda dos Quinimuras, 187 - CEP 04068-900 - São Paulo, SP<br/>
			CNPJ/MF nº62.173.620/0001-80.
		</div>
		<div class="col-xs-12 col-md-6">
			<b>Serasa Experian - São Carlos</b><br>
			Av. Doutor Heitor José Reali, 360 - CEP 13571-385 - São Carlos, SP<br/>
			CNPJ/MF nº62.173.620-0093-06.
		</div>
		<div class='copyright col-xs-12'>
			©2017 Experian Information Solutions, Inc. Experian Marketing Services All rights reserved.<br><br>
			Experian and the Experian marks used herein are service marks or registered trademarks of Experian Information Solutions, Inc. Other<br>
			product and company names mentioned herein are the property of their respective owners.
		</div>
	</div>
	<div class="hidden" style="display:none;width:1px;height:1px;" id="tmp_news"></div>
</div>
<div class="col-xs-12 box_end_f visible-xs">
	<div class="container">
		<div class='copyright col-xs-12'>
			Experian and the Experian marks used herein are service marks or registered trademarks of Experian Information Solutions, Inc. Other<br>
			product and company names mentioned herein are the property of their respective owners.
		</div>
		<div class="col-xs-12 col-md-6">
			<b>Serasa Experian - São Paulo</b><br>
			Alameda dos Quinimuras, 187 - CEP 04068-900 - São Paulo, SP<br/>
			CNPJ/MF nº62.173.620/0001-80.
		</div>
		<div class="col-xs-12 col-md-6">
			<b>Serasa Experian - São Carlos</b><br>
			Av. Doutor Heitor José Reali, 360 - CEP 13571-385 - São Carlos, SP<br/>
			CNPJ/MF nº62.173.620-0093-06.
		</div>
	</div>
	<div class="hidden" style="display:none;width:1px;height:1px;" id="tmp_news"></div>
</div>
<div style="clear: both;"></div>
<div class="hide_c mobile_page">
	<div class="mobile_page_item animated fadeInLeft form_mobile_go hide_c">
		<?php //echo do_shortcode('[contact-form-7 id="463" title="Cadastro_Cliente_Mobile"]'); ?>
		<?php echo do_shortcode('[form_mobile form=mobile origem=site]'); ?>
	</div>
	<div class="mobile_page_item animated fadeInLeft mobile_menu_list hide_c"></div>
	<div class="mobile_page_item animated fadeInLeft mobile_search hide_c">
		<div class="bg_fff">
			<form class="margin_t_50 margin_l_30" action="<?php echo site_url();?>/busca">
				<div class="w_200 left margin_r_10">
					<input type="search" autocomplete="off" class="input_c" name="se" placeholder="Buscar por...">
				</div>
				<input type="submit" class=" margin_l_10 opacity sprite pointer btn_search left" value="" placeholder="Buscar por...">
			</form>
		</div>
	</div>
</div>
<?php
echo get_field('footer_modal', get_option('page_on_front'));
?>
<div class="popover bottom">
	<div class="arrow"></div>
	<div class="popover-content"></div>
</div>

<!-- Modal BROWSERS SERASA -->
<div class="se_brck">
	<div class="modal fade row" id="modal-brcheck" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content modal-content-browser">
				<div class="modal-header modal-header-browser">
					<button type="button" class="close btn-close-modal" data-dismiss="modal" aria-label="Close" onclick="deleteAttribute()">&nbsp;</button>
					<h4 class="modal-title" id="myModalLabel">Modal title</h4>
				</div>
				<div class="modal-body col-md-7" id="modal-browser">

				</div>
				<div class="modal-footer modal-footer-browser col-md-6">
					<a href="//www.serasaexperian.com.br/segurancaweb" type="button" class="btn btn-default btn-footer-browser" style="margin-top: 0;"><strong class="color-footer">Clique aqui para mais informações</strong></a>
					<a href="https://sitenet.serasa.com.br/Logon" target="_blank" type="button" class="btn btn-primary bg-blue btn-footer-browser" data-dismiss="modal" aria-label="Close" onclick="deleteAttribute()"><strong class="color-footer">Mais tarde</strong></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /Modal BROWSERS SERASA -->

<script>
    var glbIOne = {
        score: false,
        produto: 'Tims Product',
        otherParam: 'Just a test',
        export_data:{}
    };
</script>
<script>$=jQuery;</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.1/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo $template_url; ?>/js/jquery.mask.min.js"></script>
<script type="text/javascript" src="<?php echo $template_url; ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $template_url; ?>/js/main.js"></script>
<script type="text/javascript" src="<?php echo $template_url; ?>/js/modal-browser.js"></script>
<script type="text/javascript" src="<?php echo $template_url; ?>/js/platform.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        init('<?php echo $host;?>');
    });
</script>
<!-- <?php echo $_SERVER ['SERVER_ADDR']; ?> -->

<div class="visible-xs visible-sm" style="margin-left: 30px;">
	<!--- DO NOT EDIT - GlobalSign SSL Site Seal Code - DO NOT EDIT ---><table width=125 border=0 cellspacing=0 cellpadding=0 title="CLICK TO VERIFY: This site uses a GlobalSign SSL Certificate to secure your personal information." ><tr><td><span id="ss_img_wrapper_gmogs_image_125-50_en_dblue"><a href="https://www.globalsign.com/" target=_blank title="GlobalSign Site Seal" rel="nofollow"><img alt="SSL" border=0 id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_125-50_en.gif"></a></span><script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_125-50_en_dblue.js"></script></td></tr></table><!--- DO NOT EDIT - GlobalSign SSL Site Seal Code - DO NOT EDIT --->
</div>

</body>
</html>