<?php

$postQueried = get_queried_object();

global $post;
$post = $postQueried;

?>
<div class="wrap-alternative-breadcrumb">
	<div class="container">
		<div class="row"> 
			<div class="col-xs-12">
				<div class="wrap-alternative-breadcrumb__items">
					<a href="<?php echo get_site_url();?>/decisao" class="wrap-alternative-breadcrumb__item">Home > </a>

					<?php 
						if( is_single() ) :
						$post_title = $post->post_title;
					?>
					<span class="wrap-alternative-breadcrumb__item wrap-alternative-breadcrumb__atual-page"><?php echo $post_title; ?></span>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>