<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Serasa_Vitrine_Da
 * @subpackage Serasa_Vitrine_Da/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Serasa_Vitrine_Da
 * @subpackage Serasa_Vitrine_Da/includes
 * @author     Equipe Tecnobiz <desenv@tecnobiz.com.br>
 */
class Serasa_Vitrine_Da_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
