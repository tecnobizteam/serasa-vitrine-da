<?php
include_once 'header-interna.php';
include_once 'breadcrumb.php';
include_once 'banner-interna.php';

$url_parts = Serasa_Vitrine_Da_Public::get_parts_url();
$category = $url_parts['category_exists'];
$number = $url_parts['number_page'];
$termTaxonomy = $url_parts['category_page'];
$termsTaxonomy = array($termTaxonomy);
$pos = $url_parts['page_exists'];

if (!$pos === false && $category === false) {
	$posts = Serasa_Vitrine_Da_Public::get_posts_blog(array(),$number);
	$total_pages = $posts['total_pages'];
	$posts_query = $posts['wp_query'];
} else if (!$category === false){
	$posts = Serasa_Vitrine_Da_Public::get_posts_blog($termsTaxonomy,$number);
	$total_pages = $posts['total_pages'];
	$posts_query = $posts['wp_query'];
}
else{
	$posts = Serasa_Vitrine_Da_Public::get_posts_blog();
	$total_pages = $posts['total_pages'];
	$posts_query = $posts['wp_query'];
}

global $wp_query;

?>

	<section class="posts">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-8">
					<div class="posts__list">
						<?php if ( $posts_query->have_posts() ) :
                            while( $posts_query->have_posts() ): $posts_query->the_post(); ?>
								<div class="posts__list__block">
									<div class="col-sm-12 col-md-5 col-lg-4">
										<div class="posts__list__block__image"> 
											<a href="/decisao/blog/<?php echo $post->post_name?>">
												<?php the_post_thumbnail(); ?> 
											</a>
										</div>
									</div>
									<div class="col-sm-12 col-md-7 col-lg-8">
										<div class="posts__list__block__content">	
											<h2 class="posts__list__block__content-title">
												<a href="/decisao/blog/<?php echo $post->post_name?>/">
													<?php the_title(); ?>
												</a>
											</h2>
											<div class="posts__list__block__content-date">
												<p><?php the_date('d F Y'); ?></p>
											</div>
											<div class="posts__list__block__content-subtext">
												<?php
													the_excerpt();
												?>
											</div>
											<p class="posts__list__block__content-more">
												<a href="/decisao/blog/<?php echo $post->post_name?>" rel="bookmark" title="<?php the_title();?>">Continuar Lendo >
												</a>
											</p>
										</div>
									</div>
								</div>
								<?php
								wp_reset_postdata();
                            endwhile;
						endif; ?>
					</div>
				</div>
                <!-- SIDEBAR -->
                <div class="col-xs-12 col-sm-4">
					<?php include_once( 'sidebar.php' ) ?>
                </div>
                <div class="col-xs-12">
                    <nav class="pagination">
	                    <?php Serasa_Vitrine_Da_Public::wpbeginner_numeric_posts_nav($total_pages, $number); ?>
                    </nav>
                </div>
            </div>
		</div>
	</section>
<?php include_once 'footer-interna.php'; ?>