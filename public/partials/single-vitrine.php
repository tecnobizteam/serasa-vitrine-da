<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Serasa_Vitrine
 * @subpackage Serasa_Vitrine/public/partials
 */

//Post da página atual
$postQueried = get_queried_object();

//Guarda valor do campo de anexo
$anexo = get_field( 'anexo_da_solucao' );
$telefone = get_field('telefone');
$video = get_field( 'video_exibicao' );
$site = get_field( 'site' );
$lead = get_field( 'lead' );
$texto_superior_site = get_field( 'texto_superior_site' );
$texto_superior_video = get_field( 'texto_superior_video' );
$posts_exibe = get_field( 'solucoes_exibicao' );
$exibir_outro_post = get_field( 'exibir_conteudo_do_outro_post' );


//Busca o header interno
include 'header-interna.php';
global $post;
$post = $postQueried;

?>

    <div id="vitrine-produtos">

        <div class="bg_m_branco texto-destaque header">

            <div class="container a-left">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="a-left"><?php echo $post->post_title?></h1>
                        <p class="p-text-interno"><?php echo get_field('subtitulo')?></p>
                        <a href="#text-anchor" class=" im btn link-scroll">Quero mais informações</a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12">
            <?php include_once( 'alternative-breadcrumb.php' ); ?>
        </div>

        <div class="container mt32 bottom">
            <div class="col-xs-12 col-md-7 no-pd-left-right">
                <div class="content__vitrine">
                    <?php
                        // check if the repeater field has rows of data
                        if( have_rows('bloco_conteudo') ):
                            // loop through the rows of data
                            while ( have_rows('bloco_conteudo') ) : the_row();?>
                                <div class="content_vitrine_separator">
                                    <p class="content__vitrine__title"><?php echo get_sub_field( 'titulo' ) ?></p>
                                    <?php the_sub_field('conteudo');?>
                                </div>
                            <?php endwhile;
                        else :
                            // no rows found
                         endif;
                    ?>

	                <?php
	                if ($exibir_outro_post):?>
                        <div class="row">
                            <div class="col-lg-12">
                                <p><?php echo get_field( 'texto_conteudo' ); ?></p>
                                <a class="clique_saiba_mais" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
					                <?php echo get_field( 'texto_link' );?>
                                </a>
                                <div class="collapse" id="collapseExample">
                                    <p class="another_post_title"><?php echo get_field('titulo_outro_post'); ?></p>
                                    <div>
		                                <?php
		                                // check if the repeater field has rows of data
		                                if( have_rows('bloco_conteudo_2') ):
			                                // loop through the rows of data
			                                while ( have_rows('bloco_conteudo_2') ) : the_row();?>
                                                <div class="content_vitrine_separator">
                                                    <p class="content__vitrine__title"><?php echo get_sub_field( 'titulo' ) ?></p>
					                                <?php the_sub_field('conteudo');?>
                                                </div>
			                                <?php endwhile;
		                                else :
			                                // no rows found
		                                endif;
		                                ?>
                                    </div>
                                </div>
                            </div>
                        </div>
		                <?php
	                endif;
	                ?>

                </div>

	            <?php if (function_exists('get_field') &&
	                      get_field('duvidas-frequentes-botao-texto') &&
	                      '' !== get_field('duvidas-frequentes-botao-texto') &&
	                      get_field('duvidas-frequentes-botao-url') &&
	                      '' !== get_field('duvidas-frequentes-botao-url')) : ?>
                        <div id="perguntas-text" class="separator">
                            <span>Principais Perguntas</span>
                        </div>
	            <?php endif ?>

                    <?php $repeater_slug = 'duvidas-frequentes' ?>
                        <div class="accordion-duvidas">
                            <?php include 'accordion-blue.php' ?>
                        </div>

            </div>

            <div class="col-xs-12 col-md-12 no-pd-left-right visible-xs">
                <div class="content__vitrine"><?php
                    if( have_rows( 'bloco_icones_texto') ):
                        while( have_rows('bloco_icones_texto' ) ): the_row();?>
                            <p class="content__vitrine__title content_dinamyc"><?php echo get_sub_field( 'titulo_principal' ); ?></p><?php
                            if( have_rows( 'tipo_solucao_repeater' ) ): $count = 0; $column = '';
                                while ( have_rows( 'tipo_solucao_repeater' ) ): the_row();?>
                                    <p class="main-af__title"><?php echo get_sub_field( 'titulo_solucao' ); ?></p>
                                    <div class="row">
                                    <div class="col-lg-12">
                                        <?php  $count = count( get_sub_field('detalhes_solucao') );
                                        $column = 12/$count;
                                        $class_column = 'col-xs-12 col-sm-12 col-lg-' .$column. ' col-md-'. $column;
                                        ?>
                                        <div class="row"><?php
                                            while ( have_rows('detalhes_solucao') ) : the_row();?>
                                                <div class="<?php echo $class_column;?>">
                                                <div class="main-af__alert main-af__alert--obito" style="background-image: url('<?php echo get_sub_field( 'icone__solucao' )?>')">
                                                    <p class="main-af__alert--subtitle"><?php echo get_sub_field( 'titulo_solucao' ); ?></p>
                                                    <p class="main-af__alert--body"><?php echo get_sub_field( 'texto_solucao' ); ?></p>
                                                </div>
                                                </div><?php
                                            endwhile; ?>
                                        </div>
                                    </div>
                                    </div><?php
                                endwhile;
                            endif;
                        endwhile;
                    else:
                    endif; ?>
                </div>
            </div>

            <div class="col-xs-12 col-md-5 mobile-mt16 no-pad-left-mobile">
                <div class="filtros form-video">
                    <h3 class="titulo"><?php  echo get_field( 'texto_superior_video' );?></h3>
                    <div class="row mt16">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <?php  echo get_field( 'video_solucao' );?>
                        </div>
                    </div>
                </div>

                <div class="filtros form-filtro">
                    <div>
                        <h3 class="titulo">Download do Material</h3>
                        <div class="download active">
                            <p class="mini mt16">Para fazer o download do material completo, por favor, preencha todo os campos abaixo.</p>
                            <div class="vitrine__form">
                                <?php
                                if (function_exists('gravity_form')) {
                                    $download_form_data = array(
                                        'autodownload' => $anexo,
                                    );
                                    gravity_form(
                                        'Serasa Vitrine DA - Download',
                                        false,
                                        false,
                                        false,
                                        $download_form_data,
                                        true,
                                        100
                                    );
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row mt32 mobile-center">
                        </div>
                    </div>
                </div>

                <div class="filtros mt32 form-telefone">
                    <h3 class="titulo"><?php  echo get_field('texto_superior_telefone','option')?></h3>
                    <div class="row mt16">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <p class="mini">
                                <span class="tel"><?php echo get_field('numero_capitais','option') ?></span><br>
                                para capitais e regiões metropolitanas
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <p class="mini">
                                <span class="tel"><?php  echo get_field('numero_locais','option') ?></span><br>
                                demais localidades, <br>
                                exclusivo para chamadas de<br>
                                telefone fixo
                            </p>
                        </div>
                    </div>
                </div>

                <div class="filtros mt16 form-posts">
                    <h3 class="titulo">Conheça também</h3>
                    <div class="row mt16">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <?php  $post_objects = get_field('solucoes_relacionadas');
                            if( $post_objects ): ?>
                                <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                                    <?php setup_postdata($post); ?>
                                    <li>
                                        <a class="posts__link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </li>
                                <?php endforeach; ?>
                            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-12 no-pd-left-right hidden-xs">
                <div class="content__vitrine"><?php
                    if( have_rows( 'bloco_icones_texto') ):
                        while( have_rows('bloco_icones_texto' ) ): the_row();?>
                            <p class="content__vitrine__title content_dinamyc"><?php echo get_sub_field( 'titulo_principal' ); ?></p><?php
                            if( have_rows( 'tipo_solucao_repeater' ) ): $count = 0; $column = '';
                                while ( have_rows( 'tipo_solucao_repeater' ) ): the_row();?>
                                    <p class="main-af__title"><?php echo get_sub_field( 'titulo_solucao' ); ?></p>
                                    <div class="row">
                                    <div class="col-lg-12">
	                                    <?php  $count = count( get_sub_field('detalhes_solucao') );
	                                    $column = 12/$count;
	                                    $class_column = 'col-xs-12 col-sm-12 col-lg-' .$column. ' col-md-'. $column;
	                                    ?>
                                        <div class="row"><?php
                                            while ( have_rows('detalhes_solucao') ) : the_row();?>
                                                <div class="<?php echo $class_column;?>">
                                                <div class="main-af__alert main-af__alert--obito" style="background-image: url('<?php echo get_sub_field( 'icone__solucao' )?>')">
                                                    <p class="main-af__alert--subtitle"><?php echo get_sub_field( 'titulo_solucao' ); ?></p>
                                                    <p class="main-af__alert--body"><?php echo get_sub_field( 'texto_solucao' ); ?></p>
                                                </div>
                                                </div><?php
                                            endwhile; ?>
                                        </div>
                                    </div>
                                    </div><?php
                                endwhile;
                            endif;
                        endwhile;
                    else:
                    endif; ?>
                </div>
            </div>

            <!--  Formulário de Interesse  -->
            <div id="text-anchor" class="col-xs-12 no-pd-left-right no-pad-left form-lead">
                <div class="form-interesse">
                    <div class="col-xs-12 col-md-6 margin_column_top">
                        <p class="form-interesse__text">Ficou interessado no <br> <?php echo $post->post_title?>?</p>
                        <p class="form-interesse__text--minor">Nós entramos em contato<br> com você!</p>
                    </div>
                    <div class="col-xs-12 col-md-6 filtros">
                        <div>
                            <h3 class="titulo">Ficou interessado nesta solução?</h3>
                            <div class="download active">
                                <p class="mini mt16">Preencha todos os campos abaixo e entraremos em contato.</p>
                                <div class="vitrine__form">
					                <?php
					                if (function_exists('gravity_form')) {
						                $solucao = array(
							                'solucao_escolhida' => $post->post_title,
						                );
						                gravity_form(
							                'Serasa Vitrine DA - Nós Ligamos Pra Você',
							                false,
							                false,
							                false,
							                $solucao,
							                true,
							                200
						                );
					                }
					                ?>
                                </div>
                            </div>
                            <div class="row mt32 mobile-center">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        (function($) {
            $(document).ready(verifyAttached);

            function verifyAttached(){
                //Get values of vars php
                var anexo = "<?=$anexo?>";
                var telefone = "<?=$telefone?>";
                var video = "<?=$video?>";
                var posts = "<?=$posts_exibe?>";
                var lead = "<?=$lead?>";
                var texto_site = "<?=$texto_superior_site?>";
                var texto_video = "<?=$texto_superior_video?>";

                console.log(anexo);
                //Verify if anexo has value
                if(anexo){
                    $('.form-filtro').css('display','block');
                }
                if(telefone || !telefone){
                    $('.form-telefone').css('display','none');
                }
                if(posts){
                    $('.form-posts').css('display','none');
                }else{
                    $('.form-posts').css('display','block');
                }

                if(texto_site === '' || texto_site === null || texto_site === undefined){
                    $('.form-site').css('display','none');
                }

                if(lead){
                    $('.form-lead').css('display','none');
                }else{
                    $('.form-lead').css('display','block');
                }

                if(texto_video === '' || texto_video === null || texto_video === undefined || video){
                    $('.form-video').css('display','none');
                }else{
                    $('.form-video').css('display','block');
                }

            }
        })(jQuery);
    </script>

<?php include_once 'footer-interna.php'; ?>