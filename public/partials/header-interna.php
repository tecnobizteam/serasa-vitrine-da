<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Serasa_Vitrine_Da
 * @subpackage Serasa_Vitrine_Da/public/partials
 */

$count = 0;
?>

    <!-- This file should primarily consist of HTML with a little bit of PHP. -->
    <!DOCTYPE html>
    <!--[if IE 6]>
    <html id="ie6" <?php language_attributes(); ?>>
    <![endif]-->
    <!--[if IE 7]>
    <html id="ie7" <?php language_attributes(); ?>>
    <![endif]-->
    <!--[if IE 8]>
    <html id="ie8" <?php language_attributes(); ?>>
    <![endif]-->
    <!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title>Decision Analytics - Serasa Experian</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="robots" content="follow, all" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
        <link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
        <link rel="shortcut icon" href="/wp-content/themes/serasaexperian/favicon.ico" type="image/x-icon">
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion_async.js" charset="utf-8"></script>
        <?php
        if ( is_plugin_active('gravityforms/gravityforms.php') ) {
            $newsletter_form = gravity_form( 'Newsletter', false, false, false, false, true, 10, false );
            gravity_form_enqueue_scripts( $newsletter_form, true );
        }
        wp_head();
        ?>
    </head>
<body>
    <?php  
        if($_SERVER['HTTP_HOST'] == 'https://www.serasaexperian.com.br') : ?>
            <!--GTM--><!-- Serasa Experian Google Tag Manager NOVO -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NZL99W" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>// <![CDATA[
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NZL99W');
        // ]]></script>
        <!-- End Google Tag Manager -->
        
    <?php endif; ?>
        
<?php
/// TRACKER DO GA
echo get_field('gtm', 37, false);
//@include('ga.php');
?>
    <div class="header overflow">
        <div class="ult_top_menu container">
            <div class="col-xs-12 visible-xs visible-sm">
                <a href="http://www.serasaconsumidor.com.br/" target="_blank">Consumidor</a>
                <a href="/home/empresa/" target="_blank">Empresas</a>
                <a href="https://www.experianplc.com/about-experian/worldwide-locations.aspx" target="_blank">Sites Globais</a>
                <a href="/sobre-a-serasa-experian/" target="_blank">Sobre</a>
            </div>
            <div class="row">
                <div class="col-md-12 hidden-xs hidden-sm">
                    <a href="<?php echo site_url();?>" target="_blank">Serasa Experian</a>
                    <a href="http://www.serasaconsumidor.com.br/" target="_blank">Serasa Consumidor</a>
                    <a class="right" href="https://www.experianplc.com/about-experian/worldwide-locations.aspx" target="_blank">Global Sites</a>
                    <a class="right" href="https://www.serasaexperian.com.br/auto-atendimento/" target="_blank">Atendimento</a>
                    <a class="right" href="/sobre-a-serasa-experian/" target="_blank">Sobre a Serasa Experian</a>
                </div>
            </div>
        </div>
    </div>
<?php
$menu = false;
    if ( is_singular( 'page' ) && get_field( 'ocultar_menu' ) ) {
        //break;
    }
    ?>
    <div class="nav bg_fff hidden-xs hidden-sm">
        <div id="menu" class="container relative">
            <div class="row">
                <?php $hn = is_front_page() ? '1' : '3'; ?>
                <div class='m_items col-sm-7'>
                    <a href="<?php echo get_site_url();?>/decisao" class='left'><h<?php echo $hn; ?> style="width: 141px;height: 60px;overflow: hidden;text-indent: 500%;white-space: nowrap;background:url(<?php bloginfo('template_url');?>/images/logo.png);display:inline-block;margin-top: 17px;">Serasa Experian</h<?php echo $hn; ?>></a>
                    <div class="inline_block margin_l_20">
                        <a class="text-nav-item inline_block  padding_t_35 padding_b_25 margin_r_15 margin_l_15 left" href="<?php echo get_site_url();?>/decisao">Início</a>
                        <a id="menu-solucoes" href="<?php echo get_site_url();?>/decisao/" class="text-nav-item inline_block padding_t_35 padding_b_25 margin_r_15 margin_l_15 left" href="">Nossas Soluções</a>
                        <a class="text-nav-item inline_block  padding_t_35 padding_b_25 margin_r_15 margin_l_15 left" href="<?php echo get_site_url();?>/academia" target="__blank">Academia Serasa Experian</a>
                        <a class="text-nav-item inline_block  padding_t_35 padding_b_25 margin_r_15 margin_l_15 left" href="<?php echo get_site_url();?>/decisao/blog/">Blog</a>
                    </div>
                </div>
                <div class='m_controls margin_t_20 col-sm-5'>
                    <div class="pull-right">
                        <a href="https://sitenet.serasa.com.br/Logon/" target="_blank" class='btn margin_r_20' style="padding: 12px 45px">Acessar</a> <form action="<?php echo site_url();?>/busca" class="inline_block" style="display: none;">
                            <input name="se" type="text" autocomplete="off" placeholder="Busca..." />
                            <input type="submit" value="" class="opacity pointer btn_search">
                        </form>
                        <button type='button' class='btn_show_search pointer opacity'><img src='<?php echo get_template_directory_uri(); ?>/images/lupa-menu.png' alt='Buscar' /></button>
                    </div>
                </div>
                <div style="clear:both"></div>
                <div class="col-sm-12">
                    <div id="menu-dropdown-solucoes" class="dropdown-main-menu">
                        <?php  $tipos_solucao = Serasa_Vitrine_Da_Public::get_information_of_public_taxonomy('tipo-de-solucao-da');
                        foreach ($tipos_solucao as $tipo_solucao):?>
                            <?php  if($count === 0):?>
                                <div class="row">
                            <?php endif; $count++?>
                            <div class="col-sm-4">
                                <img class="dropdown-main-menu__img-list" src="<?php echo $tipo_solucao['image']; ?>" alt="">
                                <div class="dropdown-main-menu__list">
                                    <p class="dropdown-main-menu__list__title"><?php echo $tipo_solucao['name'];?></p>
                                    <ul class="dropdown-main-menu__ul">
                                        <?php  $posts = Serasa_Vitrine_Da_Public::get_posts_by_term('tipo-de-solucao-da', $tipo_solucao['slugTypeSolution']);
                                        foreach ($posts as $post):?>
                                            <li><a class="dropdown-main-menu__link" href="<?php echo get_permalink($post->ID)?>"><?php echo $post->post_title?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                            <?php if($count === 3): ?>
                                </div>
                            <?php $count = 0;  endif;?>
                        <?php endforeach;?>
                        <?php  if ($count > 0): ?>
                    </div>
                    <?php  endif;?>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--    MOBILE     -->
    <?php
if( 0 === 0 ) {
    ?>
    <div class="nav-mobile visible-xs visible-sm">
        <div class="row" style="margin-right: 0;margin-left: 0;">
            <h1 class="col-xs-4">
                <a href="/decisao/"><img class="margin_top_10_nav" src="<?php echo get_template_directory_uri(); ?>/images/logo-mobile.png" alt="Serasa Experian"></a>
            </h1>
            <div class="col-xs-8">
                <label for="menu-dropdown"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-menu.png" alt="Abrir o menu"></label>
            </div>
        </div>
        <div class="row" style="margin-right: 0;margin-left: 0;">
            <input type="checkbox" id="menu-dropdown">
            <ul class="menu-items col-xs-12">
                <li>
                    <form action="<?php echo site_url();?>/busca" class="" style="">
                        <input type="search" name="se" placeholder="Busca...">
                    </form>
                </li>
                <li>
                    <a class="text-nav-item m_item" href="<?php echo get_site_url();?>/academia" target="__blank">Academia Serasa Experian</a>
                </li>
                <li>        
                    <a class="text-nav-item m_item" href="<?php echo get_site_url();?>/decisao/blog/">Blog</a>
                </li>
                <?php foreach($menu as $texto => $link) : ?>
                    <li>
                        <a class="m_item" href="<?php echo $link; ?>"><?php echo $texto; ?></a>
                    </li>
                <?php endforeach; ?>
                <?php  $tipos_solucao = Serasa_Vitrine_Da_Public::get_information_of_public_taxonomy('tipo-de-solucao-da');
                foreach ($tipos_solucao as $tipo_solucao):?>
                    <li>
                        <a class="menu_mobile_link__principal"><?php echo $tipo_solucao['name'];?></a>
                        <?php  $posts = Serasa_Vitrine_Da_Public::get_posts_by_term('tipo-de-solucao-da', $tipo_solucao['slugTypeSolution']);
                        foreach ($posts as $post):?>
                            <li class="menu_mobile_link"><a href="<?php echo get_permalink($post->ID)?>"><?php echo $post->post_title?></a>
                        <?php  endforeach;?>
                    </li>
                <?php  endforeach;?>
                <li>
                    <a href="https://www.serasaexperian.com.br/auto-atendimento/" class="m_item">Atendimento</a>
                </li>
                <li>
                    <a href="https://sitenet.serasa.com.br/Logon/" class="m_item">Acessar</a>
                </li>
            </ul>
        </div>
    </div>
    </body>
    <?php
}
