<?php
//Post da página atual
$postQueried = get_queried_object();

global $post;
$post = $postQueried;

?>
<div class="wrap-breadcrumb">
	<div class="container">
		<div class="row"> 
			<div class="col-xs-12">
				<div class="wrap-breadcrumb__items">
					<a href="<?php echo get_site_url();?>/decisao" class="wrap-breadcrumb__item">Home</a>

					<?php 
						if( is_single() ) {
						$post_title = $post->post_title;
					?>
					
					<a href="<?php echo get_site_url();?>/decisao/blog/" class="wrap-breadcrumb__item">Blog</a>
					<span class="wrap-breadcrumb__item wrap-breadcrumb__atual-page"><?php echo $post_title; ?></span>

					<?php } else if (is_archive() ) { ?>

					<span class="wrap-breadcrumb__item wrap-breadcrumb__atual-page">Blog</span>

					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>