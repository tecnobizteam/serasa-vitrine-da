=== Datalabs ===

Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Este plugin tem a funcionalidade de criar a página Serasa Vitrine e as páginas internas de cada soliução

== Descrição ==

As funcionalidades deste plugin incluem:

* Criar página inicial que lista todas as soluções.

== Instalação ==

1. Adicionar o arquivo 'datalabs.zip' à pasta '/wp-content/plugins/'.
2. Descompactar o arquivo e ativar o plugin.
3. Com o Plugin ATIVADO, acessar o menu 'Campos Personalizados', selecionar a guia 'Sincronização Disponível' e sincronizar todos os campos disponíveis.
4. Criar nova página que tenha como Título "Serasa Vitrine DA" e que o link permanente dele tenha como slug o recurso: /serasa-vitrine-da/.

