<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Serasa_Vitrine_Da
 * @subpackage Serasa_Vitrine_Da/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Serasa_Vitrine_Da
 * @subpackage Serasa_Vitrine_Da/includes
 * @author     Equipe Tecnobiz <desenv@tecnobiz.com.br>
 */
class Serasa_Vitrine_Da {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Serasa_Vitrine_Da_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'serasa-vitrine-da';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Serasa_Vitrine_Da_Loader. Orchestrates the hooks of the plugin.
	 * - Serasa_Vitrine_Da_i18n. Defines internationalization functionality.
	 * - Serasa_Vitrine_Da_Admin. Defines all hooks for the admin area.
	 * - Serasa_Vitrine_Da_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		/**
		 * Carrega as classes por meio do autoload PSR-4.
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'vendor/autoload.php';

		Brain\Cortex::boot();

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-serasa-vitrine-da-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-serasa-vitrine-da-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-serasa-vitrine-da-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-serasa-vitrine-da-public.php';

		$this->loader = new Serasa_Vitrine_Da_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Serasa_Vitrine_Da_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Serasa_Vitrine_Da_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Serasa_Vitrine_Da_Admin( $this->get_plugin_name(), $this->get_version() );

        $this->loader->add_action( 'init', $plugin_admin, 'createPostTypeSolucoes');
        $this->loader->add_action( 'init', $plugin_admin, 'registerTaxonomyTypeSolucao');
        $this->loader->add_action( 'init', $plugin_admin, 'createPostTypeBlog');
        $this->loader->add_action( 'init', $plugin_admin, 'registerTaxonomyTypeBlog');
        $this->loader->add_action('init', $plugin_admin, 'registerPluginPageOptions');
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
        $this->loader->add_filter( 'acf/settings/load_json', $plugin_admin, 'acfJsonLoad' );
        $this->loader->add_filter( 'excerpt_length', $plugin_admin, 'custom_excerpt_length');
        $this->loader->add_filter( 'excerpt_more', $plugin_admin, 'themify_custom_excerpt_more');

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Serasa_Vitrine_Da_Public( $this->get_plugin_name(), $this->get_version() );

        $this->loader->add_filter('page_template', $plugin_public, 'createPageTemplate');
        $this->loader->add_filter( 'archive_template', $plugin_public, 'createCustomPostArchive');
        $this->loader->add_filter( 'single_template', $plugin_public, 'createCustomPostSinglesBlogDA');
        $this->loader->add_filter('single_template', $plugin_public, 'createCustomPostSingles');
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'cortex.routes',$plugin_public, 'routeUrlSolucoes' );
		$this->loader->add_action( 'wp_head', $plugin_public, 'wpb_track_post_views');
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Serasa_Vitrine_Da_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
