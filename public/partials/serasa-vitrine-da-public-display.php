<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Serasa_Vitrine_Da
 * @subpackage Serasa_Vitrine_Da/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php
    if( have_rows('slides_home','option') )
    {
    ?>
    <div class="row" style="margin-left: 0;margin-right: 0">
        <div class="container_r_slide">
            <div class="slide_home parallaxs overflow">
                <div id="owl-init" class="owl-carousel owl-theme">
                    <?php
                    $html = '';
                    while ( have_rows('slides_home','option') )
                    {
                        the_row();

                        $imagem = get_sub_field('imagem');
                        $data_art = get_sub_field('data_art');
                        $thumb 	= get_sub_field('thumb');
                        $thumb 	= isset($thumb['url']) && $thumb['url'] ? $thumb['url'] : $imagem['url'];

                        if(get_sub_field('link') && !get_sub_field('texto_btn')){
                            $html .= '<a href="'.get_sub_field('link').'" target="'.get_sub_field('nova_aba').'" class="block relative item c_fff">';
                        }

                        $html .= '<div class="slider-container" style="background-image: url(' . $imagem['url'] . ')">';

                        if(get_sub_field('data_art')){
                            $html .= "<div class='container data-art' style='background-image: url(" . $data_art['url'] . ");'>";
                        }else{
                            $html .= "<div class='container' style='padding-left: 0;'>";
                        }

                        if(get_sub_field('titulo')){
                            $html .= '<span class="block title_slide bounceInDown">'.get_sub_field('titulo').'</span>';
                        }

                        if(get_sub_field('texto')){
                            $html .= '<span class="block text_slide bounceInDown">'.get_sub_field('texto').'</span>';
                        }

                        if(get_sub_field('texto_btn')){
                            if(get_sub_field('link')) {
                                $html .= '<a href="'.get_sub_field('link').'" target="'.get_sub_field('nova_aba').'" class="relative item c_fff">';
                            }

                            $html .= '<br/><span class="f12 c_fff btn_' . get_sub_field('cor_btn') . ' c_fff_hover btn_slide bounceInUp" >'.get_sub_field('texto_btn').'&nbsp&nbsp></span>';

                            if(get_sub_field('link')) {
                                $html .= '</a>';
                            }
                        }

                        $html .= '</div>';
                        $html .= '</div>';

                        if(get_sub_field('link') && !get_sub_field('texto_btn')){
                            $html .= '</a>';
                        }
                    }
                    echo $html;
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
}