# Serasa Vitrine DA
[![N|Solid](https://www.serasaexperian.com.br/wp-content/themes/serasaexperian/images/logo.png)](https://www.serasaexperian.com.br/)

Serasa Vitrine DA tem como funcionalidade criar o Post Type de Soluções DA e a criação da página Serasa Vitrine DA

# As funcionalidades deste plugin incluem
  - Criar o Custom Post Type de Soluções DA
  - Criar os campos do acf que integram juntamente com o Custom Post Type
  - Formulários de Download do Material e Contato (Serasa Vitrine DA - Nós ligamos para você)

# Instalação
  - Adicionar o arquivo 'serasa-vitrine-da.zip' à pasta '/wp-content/plugins/'.
  - Descompactar o arquivo e ativar o plugin.
  - Com o Plugin ATIVADO, acessar o menu 'Campos Personalizados', selecionar a guia 'Sincronização Disponível' e sincronizar todos os campos disponíveis.
  - Selecionar a opção 'Importar/Exportar' no menu 'Formulários' e importar o arquivo .json que se encontra na pasta '/forms' na raiz do Plugin.
 - Criar a página inicial: Serasa Vitrine DA, com o slug /decisao/
  



