<?php include_once 'header-interna.php'; ?>
	<?php include_once 'breadcrumb.php' ?>
<?php

//Post da página atual
$postQueried = get_queried_object();

global $post;
$post = $postQueried;

//Contabiliza quantas vezes o post foi visualizado
Serasa_Vitrine_Da_Public::wpb_set_post_views($post->ID);

?>
	<section class="post">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-8">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="post__title"><?php the_title();?></h1>
                        </div>
                        <div class="col-xs-12">
                            <div class="post__featured-image">
			                    <?php if( has_post_thumbnail() ): ?>
                                <img src="<?php echo get_the_post_thumbnail_url(); ?>" style="height: auto; width: 400px;" class="img-responsive attachment-post-thumbnail size-post-thumbnail wp-post-image" sizes="(max-width: 900px) 100vw, 900px">
                                <?php  endif; ?>
                            </div>
                            <div class="post__subtext">
			                    <?php the_content();?>
                            </div>
                            <div class="post__prev-and-next">
                            	<?php
                            		// Adiciona o prev e o next aos posts
                            		Serasa_Vitrine_Da_Public::prev_and_next_posts();
                            	?>	
                            </div>
                        </div>
                    </div>
				</div>

				<!-- SIDEBAR -->
				<div class="col-xs-12 col-sm-4">
					<?php include_once( 'sidebar.php' ) ?>
				</div>	
			</div>
		</div>
	</section>
	
	<section class="comments">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
				</div>
			</div>
		</div>
	</section>
<?php include_once 'footer-interna.php'; ?>

<script type="text/javascript">
    (function($) {
        $(document).ready(verifyAttached);

        function verifyAttached(){
            //Get values of vars php
            var anexo = "<?=$anexo?>";

            //Verify if anexo has value
            if(anexo){
                $('.form-filtro').css('display','block');
            }

        }
    })(jQuery);
</script>
