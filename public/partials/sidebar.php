<?php

//Pega anexo do blog
$anexo = get_field( 'anexo_do_blog' );
$mostViewPosts = Serasa_Vitrine_Da_Public::get_most_view_posts();

//Pega termos existentes na categoria
$terms_blog = Serasa_Vitrine_Da_Public::get_terms_blog('tipo-de-post-da');
?>


<div class="filtros form-filtro form-blog-download">
	<div>
        <h3 class="titulo">Download do Material</h3>
        <div class="download active">
            <p class="mini mt16">Para fazer o download do material completo, por favor, preencha todo os campos abaixo.</p>
            <div class="vitrine__form">
                <?php
                if (function_exists('gravity_form')) {
                    $download_form_data = array(
                        'autodownload' => $anexo,
                    );
                    gravity_form(
                        'Serasa Vitrine DA Blog - Download',
                        false,
                        false,
                        false,
                        $download_form_data,
                        true,
                        100
                    );
                }
                ?>
            </div>
        </div>
        <div class="row mt32 mobile-center">
        </div>
    </div>
</div>

<?php  if(!empty($terms_blog)):?>
<div class="termsPosts">
    <p class="termsPosts__title">Categorias</p>
	<?php foreach ($terms_blog as $term): ?>
        <?php if(!$term->count == 0):?>
        <div class="termsPosts__singlePost">
            <a class="termsPosts__link" href="/decisao/blog/categoria/<?php echo $term->slug; ?>">
                <p class="termsPosts__title--post"><?php echo $term->name; ?> (<?php echo $term->count; ?>)</p>
            </a>
        </div>
	<?php endif; endforeach; ?>
</div>
<?php endif; ?>

<div class="popularPosts">
    <p class="popularPosts__title">Mais lidos</p>
	<?php  foreach ($mostViewPosts as $mostViewPost): ?>
        <div class="popularPosts__singlePost">
            <a class="popularPosts__link" href="/decisao/blog/<?php echo $mostViewPost->post_name; ?>">
                <p class="popularPosts__title--post"><?php echo $mostViewPost->post_title; ?></p>
                <p class="popularPosts__readMore">Leia mais ></p>
            </a>
        </div>
	<?php  endforeach; ?>
</div>

