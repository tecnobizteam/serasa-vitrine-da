(function($){
    "use strict";

    //Variáveis do menu
    var $menu_dropdown,
        $menu,
        $menu_solucoes;


    $(document).ready(function(){
        _initElements();
        _initEvents();
    });

    /**
     * Inicializa todos os elementos utilizados no módulo.
     */
    function _initElements()
    {
        $menu = $('#menu');
        $menu_solucoes = $menu.find('#menu-solucoes');
        $menu_dropdown = $menu.find('#menu-dropdown-solucoes');
    }

    /**
     * Inicializa todos os eventos utilizados no módulo.
     */
    function _initEvents()
    {
        $menu_solucoes.on(
            'mouseover',
            _hoverMenuSolucoes
        );

    }

    /**
     * Chamado ao dar hover no menu "Nossas Soluções"
     */
    function _hoverMenuSolucoes()
    {
        $menu_dropdown.fadeIn();
        setTimeout(_hoverMenuDropDown, 1000);
    }

    /**
     * Chamado ao hover do menu dropdown
     */
    function _hoverMenuDropDown() {
        if($menu_dropdown.is(":hover")){
            $menu_dropdown.show();
            setTimeout(_hoverMenuDropDown,100);
        }else if($menu_solucoes.is(":hover")){
            $menu_dropdown.show();
            setTimeout(_hoverMenuDropDown,100);
        }else{
            $menu_dropdown.fadeOut();
            return false;
        }
    }

})(jQuery);
