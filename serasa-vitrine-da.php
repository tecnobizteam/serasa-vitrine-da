<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://developer.wordpress.org/
 * @since             1.0.0
 * @package           Serasa_Vitrine_Da
 *
 * @wordpress-plugin
 * Plugin Name:       Serasa Vitrine DA
 * Plugin URI:        https://developer.wordpress.org/plugins/the-basics/
 * Description:       Serasa Vitrine DA tem como funcionalidade criar o Post Type de Soluções DA e a criação da página Serasa Vitrine DA
 * Version:           1.0.0
 * Author:            Equipe Tecnobiz
 * Author URI:        https://developer.wordpress.org/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       serasa-vitrine-da
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-serasa-vitrine-da-activator.php
 */
function activate_serasa_vitrine_da() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-serasa-vitrine-da-activator.php';
	Serasa_Vitrine_Da_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-serasa-vitrine-da-deactivator.php
 */
function deactivate_serasa_vitrine_da() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-serasa-vitrine-da-deactivator.php';
	Serasa_Vitrine_Da_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_serasa_vitrine_da' );
register_deactivation_hook( __FILE__, 'deactivate_serasa_vitrine_da' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-serasa-vitrine-da.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_serasa_vitrine_da() {

	$plugin = new Serasa_Vitrine_Da();
	$plugin->run();

}
run_serasa_vitrine_da();
