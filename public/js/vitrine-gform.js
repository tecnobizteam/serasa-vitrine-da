(function( $ ) {
    'use strict';

    /**
     * All of the code for your public-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
	 *
	 * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
	 *
	 * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */

    $(document).ready(function(){

        var $gformButton = $('.gform_footer.top_label'),
            $checkBox = $('#checkbox-lincese'),
            $checkBox2 = $('#checkbox-lincese2'),
            $gcheckBox = $('.check__material input'),
            $gcheckBox2 = $('.check__material2 input'),
            $inputTelefone = $('.input__telefone').find('input');

        //Adiciona check automático
        $gcheckBox.prop("checked",true);
        $gcheckBox2.prop("checked",true);
        $checkBox.prop("checked",true);
        $checkBox2.prop("checked",true);

        //Set all with none
        var labels = $('.gfield_label'); //$Labels
        labels.addClass('hidelabels');


        function verifica(){
            var li = $('.gform_body li');

            if(li.hasClass('gfield_error')){

                $(".gform_body input").each(function(){
                    var li = $(this).parent().parent();
                    var label = li.children('.gfield_label');

                    if($(this).val() !== ""){
                        label.removeClass('hidelabels');
                        $(this).css("padding","12px 12px 0");
                        label.addClass('textLabel');
                    }

                    else if($(this).val() === ""){
                        $(this).css("padding","12px 12px 12px");
                        label.addClass('hidelabels');
                    }

                });
                $(".gform_body textarea").each(function(){
                    var li = $(this).parent().parent();
                    var label = li.children('.gfield_label');

                    if($(this).val() !== ""){
                        label.removeClass('hidelabels');
                        $(this).css("padding","5px 12px 0");
                        label.addClass('textLabel');
                    }

                    else if($(this).val() === ""){
                        $(this).css("padding","5px 12px 12px");
                        label.addClass('hidelabels');
                    }

                });
            }else{
                $(".gform_body input").each(function(){
                    var li = $(this).parent().parent();
                    var label = li.children('.gfield_label');

                    if($(this).val() !== ""){
                        label.removeClass('hidelabels');
                        $(this).css("padding","12px 12px 0");
                    }

                    if($(this).val() === ""){
                        $(this).css("padding","12px 12px 12px");
                        label.addClass('hidelabels');
                    }

                });
                $(".gform_body textarea").each(function(){
                    var li = $(this).parent().parent();
                    var label = li.children('.gfield_label');

                    if($(this).val() !== ""){
                        label.removeClass('hidelabels');
                        $(this).css("padding","5px 12px 0");
                    }

                    if($(this).val() === ""){
                        $(this).css("padding","5px 12px 12px");
                        label.addClass('hidelabels');
                    }

                });
            }
        }

        setInterval(verifica,500);

        //Set label fields
        $('.gform_body .ginput_container.ginput_container_text,.ginput_container.ginput_container_email,.ginput_container.ginput_container_phone').on('keyup', 'input,textarea', function(e){
            var li = $(this).parent().parent();//The li of the field
            var inputContainer = $(this).parent();//Input container
            var childrenContainer = inputContainer.children();

            if(childrenContainer.prop("tagName") == "INPUT"){
                var input = inputContainer.children('input');//The input
                var label = li.children('.gfield_label'); //$Labels
                input.css("padding","0");
                input.css("padding","12px 12px 0");
                label.removeClass('hidelabels');

                if(input.val() === ""){
                    label.addClass("hidelabels");
                    input.css("padding","0");
                    input.css("padding","12px 12px 0");
                }

            }
        });

        //Verify if check is checked
        $gformButton.on('click',function () {
            if($checkBox.is(":checked")){
                $gcheckBox.prop("checked", true);
            }
        });

        $inputTelefone.mask('(00) 0000-00000');

    });

})( jQuery );


