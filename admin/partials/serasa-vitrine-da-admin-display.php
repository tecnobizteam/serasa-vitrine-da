<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Serasa_Vitrine_Da
 * @subpackage Serasa_Vitrine_Da/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
