<?php
global $post;

$titulo = get_field( 'titulo_do_blog', 'options' );
$subtitulo = get_field( 'subtitulo_do_blog', 'options' );

?>

<section class="banner">
	<div class="container">
		<h2 class="banner__title"><?php echo $titulo;?></h2>
        <p class="banner__subtext"><?php echo $subtitulo;?></p>
	</div>
</section>