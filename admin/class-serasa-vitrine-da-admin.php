<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Serasa_Vitrine_Da
 * @subpackage Serasa_Vitrine_Da/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Serasa_Vitrine_Da
 * @subpackage Serasa_Vitrine_Da/admin
 * @author     Equipe Tecnobiz <desenv@tecnobiz.com.br>
 */
class Serasa_Vitrine_Da_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

    /**
     * Registra Custom Post Type de Soluções DA
     *
     * @since    1.0.0
     */
    public function createPostTypeSolucoes()
    {
        $labels = array(
            'name'                  => _x('Serasa Vitrine DA', 'Post Type General Name'),
            'singular_name'         => _x('Serasa Vitrine DA', 'Post Type Singular Name'),
            'menu_name'             => __('Soluções DA'),
            'name_admin_bar'        => __('Solução DA'),
            'all_items'             => __('Todas as Soluções'),
            'add_new_item'          => __('Adicionar Nova Solução'),
            'add_new'               => __('Adicionar Nova'),
            'new_item'              => __('Novo Solução'),
            'edit_item'             => __('Editar Solução'),
            'update_item'           => __('Atualizar Solução'),
            'view_item'             => __('Ver Solução'),
            'view_items'            => __('Ver Soluções'),
            'search_items'          => __('Procurar Solução'),
            'not_found'             => __('Nenhum Solução encontrado'),
            'not_found_in_trash'    => __('Nada encontrado na Lixeira'),
            'featured_image'        => __('Imagem de Destaque'),
            'set_featured_image'    => __('Escolher Imagem de Destaque'),
            'remove_featured_image' => __('Remover Imagem de Destaque'),
            'use_featured_image'    => __('Usar como Imagem de Destaque'),
            'insert_into_item'      => __('Inserir na Solução'),
            'uploaded_to_this_item' => __('Upload na Solução'),
        );

        $args = array(
            'label'                 => __('Serasa Vitrine DA', 'Serasa Vitrine DA'),
            'description'           => __('Informações das Soluções', 'Serasa Vitrine DA'),
            'labels'                => $labels,
            'supports'              => array('title', 'editor', 'thumbnail', 'revisions'),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_icon'             => 'dashicons-welcome-widgets-menus',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
            'rewrite' => array('slug' => 'decisao'),
        );

        register_post_type('serasa-vitrine-da', $args);
    }

    /**
     * Registra taxonomia Tipos de Soluções
     *
     * @since    1.0.0
     */
    public function registerTaxonomyTypeSolucao()
    {
        $labels =  array(
            'name' => __( 'Tipos de Soluções', 'vitrineSerasaDA' ), /* name of the custom taxonomy */
            'singular_name' => __( 'Tipo de Solução', 'vitrineSerasaDA' ), /* single taxonomy name */
            'search_items' =>  __( 'Pesquisar Soluções', 'vitrineSerasaDA' ), /* search title for taxomony */
            'all_items' => __( 'Todas as Soluções', 'vitrineSerasaDA' ), /* all title for taxonomies */
            'edit_item' => __( 'Editar Solução', 'vitrineSerasaDA' ), /* edit custom taxonomy title */
            'update_item' => __( 'Atualizar Solução', 'vitrineSerasaDA' ), /* update title for taxonomy */
            'add_new_item' => __( 'Adicionar Nova Solução', 'vitrineSerasaDA' ), /* add new title for taxonomy */
            'new_item_name' => __( 'Novo Nome da Solução', 'vitrineSerasaDA' ) /* name title for taxonomy */
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_admin_column' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'type-solucao-da' )
        );

        register_taxonomy('tipo-de-solucao-da','serasa-vitrine-da', $args);
    }

    /**
     * Registra Custom Post Type do Blog
     *
     * @since    1.0.0
     */
    public function createPostTypeBlog()
    {
        $labels = array(
            'name'                  => _x('Blog Serasa DA', 'Post Type General Name'),
            'singular_name'         => _x('Blog Serasa DA', 'Post Type Singular Name'),
            'menu_name'             => __('Blog Serasa DA'),
            'name_admin_bar'        => __('Blog Serasa DA'),
            'all_items'             => __('Todos os Posts'),
            'add_new_item'          => __('Adicionar Novo Post'),
            'add_new'               => __('Adicionar Novo Post'),
            'new_item'              => __('Novo Post'),
            'edit_item'             => __('Editar Posts'),
            'update_item'           => __('Atualizar Posts'),
            'view_item'             => __('Ver Post'),
            'view_items'            => __('Ver Posts'),
            'search_items'          => __('Procurar Posts'),
            'not_found'             => __('Nenhum Post encontrado'),
            'not_found_in_trash'    => __('Nada encontrado na Lixeira'),
            'featured_image'        => __('Imagem de Destaque'),
            'set_featured_image'    => __('Escolher Imagem de Destaque'),
            'remove_featured_image' => __('Remover Imagem de Destaque'),
            'use_featured_image'    => __('Usar como Imagem de Destaque'),
            'insert_into_item'      => __('Inserir no Post'),
            'uploaded_to_this_item' => __('Upload no Post'),
        );

        $args = array(
            'label'                 => __('Blog Serasa DA', 'Blog Serasa DA'),
            'description'           => __('Informações dos Posts', 'Blog Serasa DA'),
            'labels'                => $labels,
            'supports'              => array('title', 'editor', 'thumbnail', 'revisions'),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_icon'             => 'dashicons-welcome-widgets-menus',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
//            'rewrite' => array('slug' => 'blog'),
        );

        register_post_type('blog-serasa-da', $args);
    }

    /**
     * Registra taxonomia Tipos de Posts do Blog
     *
     * @since    1.0.0
     */
    public function registerTaxonomyTypeBlog()
    {
        $labels =  array(
            'name' => __( 'Categorias dos Posts', 'vitrineSerasaDA' ), /* name of the custom taxonomy */
            'singular_name' => __( 'Categoria do Post', 'vitrineSerasaDA' ), /* single taxonomy name */
            'search_items' =>  __( 'Pesquisar Categoria', 'vitrineSerasaDA' ), /* search title for taxomony */
            'all_items' => __( 'Todas Categorias', 'vitrineSerasaDA' ), /* all title for taxonomies */
            'edit_item' => __( 'Editar Categoria', 'vitrineSerasaDA' ), /* edit custom taxonomy title */
            'update_item' => __( 'Atualizar Categoria', 'vitrineSerasaDA' ), /* update title for taxonomy */
            'add_new_item' => __( 'Adicionar Nova Categoria', 'vitrineSerasaDA' ), /* add new title for taxonomy */
            'new_item_name' => __( 'Novo Nome da Categoria', 'vitrineSerasaDA' ) /* name title for taxonomy */
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_admin_column' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'type-blog-serasa-da' )
        );

        register_taxonomy('tipo-de-post-da','blog-serasa-da', $args);
    }

    /**
     * Adiciona pasta para carregamento automático dos ACF's utilizados
     *
     * @since    1.0.0
     */
    public function acfJsonLoad( $paths ) {

        // append path
        $paths[] = dirname(__DIR__) . '/acf-json';

        // return
        return $paths;

    }

    /**
     * Customização do excerpt
     *
     * @since    1.0.0
     */
    public function custom_excerpt_length( $length ) {
        return 30;
    }

    function themify_custom_excerpt_more($more) {
        global $post;
        
        return '<span class="more">'. __(' [...]', 'themify') .'</span>';
    }


    /**
     * Registra Página de Opções Referente à Página
     *
     * @since    1.0.0
     */
    public function registerPluginPageOptions()
    {
        if (function_exists('acf_add_options_page')) {
            // Add parent.
            $parent = acf_add_options_page(array(
                'page_title' => 'Serasa Vitrine DA',
                'menu_title' => 'Serasa Vitrine DA',
                'menu_slug' 	=> 'pagina-inicial',
                'redirect' => true,
            ));

            // Add sub page.
            acf_add_options_sub_page(array(
                'page_title' => 'Página Inicial',
                'menu_title' => 'Página Inicial',
                'parent_slug' => $parent['menu_slug'],
            ));

        }
    }

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Serasa_Vitrine_Da_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Serasa_Vitrine_Da_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/serasa-vitrine-da-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Serasa_Vitrine_Da_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Serasa_Vitrine_Da_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/serasa-vitrine-da-admin.js', array( 'jquery' ), $this->version, false );

	}

}
