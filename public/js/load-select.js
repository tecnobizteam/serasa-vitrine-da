(function($){
    "use strict";

    $(document).ready(function(){
        $('#sub-menu-xs').on('change', function (e) {
            $('#tabs-solution li a').eq($(this).val()).tab('show');
        });
    });

})(jQuery);
