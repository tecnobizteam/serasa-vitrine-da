<?php
/*
 * A variável '$repeater_slug' deve ser obrigatoriamente
 * instanciada antes da inclusão deste arquivo.
 * A variável deve conter o slug do repeater à ser mapeado.
 * Ver função 'extract_accordion_array' mo arquivo 'partials/conf.php'.
 */
if (! method_exists('Serasa_Vitrine_Public','extract_accordion_array') ||
    ! isset($repeater_slug) || '' == $repeater_slug) {
    $accordion_itens = array();
} else {
    $accordion_itens = Serasa_Vitrine_Public::extract_accordion_array($repeater_slug);
}
$accordion_panel_number = 0;
?>
<div class="accordion-blue panel-group" id="accordion-<?php echo $repeater_slug ?>" role="tablist" aria-multiselectable="true">
    <?php foreach ($accordion_itens as $accordion_item) : ?>
        <?php
        $accordion_panel_id = $repeater_slug . '-'. $accordion_panel_number;
        $collapse_class = $accordion_item['open'] ? 'in' : '';
        $aria_expanded = var_export($accordion_item['open'], true);
        ?>
        <div class="panel panel-default solution__accordion">
            <div class="accordion-blue__heading" role="tab" id="heading-<?php echo $accordion_panel_id ?>">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-<?php echo $repeater_slug ?>" data-target="#collapse-<?php echo $accordion_panel_id ?>" aria-expanded="<?php echo $aria_expanded ?>" aria-controls="collapse-<?php echo $accordion_panel_id ?>">
                        <?php echo array_key_exists('title', $accordion_item) ?
                            $accordion_item['title'] : '' ?>
                    </a>
                </h4>
            </div>
            <div id="collapse-<?php echo $accordion_panel_id ?>" class="panel-collapse collapse <?php echo $collapse_class ?>" role="tabpanel" aria-labelledby="heading-<?php echo $accordion_panel_id ?>">
                <div class="panel-body">
                    <?php echo array_key_exists('content', $accordion_item) ?
                        $accordion_item['content'] : '' ?>
                </div>
            </div>
        </div>
        <?php $accordion_panel_number++ ?>
    <?php endforeach ?>
</div>

<?php $repeater_slug = '' ?>
