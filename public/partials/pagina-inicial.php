<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://developer.wordpress.org/
 * @since      1.0.0
 *
 * @package    Serasa_Vitrine_Da
 * @subpackage Serasa_Vitrine_Da/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php

//Busca o header interno
include 'header-interna.php';
$count = 0;
$count_active = 0;
$count_options = 1;

if( have_rows('slides_home','option') )
{
    ?>
    <div class="row" style="margin-left: 0;margin-right: 0">
        <div class="container_r_slide">
            <div class="slide_home parallaxs overflow">
                <div id="owl-init" class="owl-carousel owl-theme">
                    <?php
                    $html = '';
                    while ( have_rows('slides_home','option') )
                    {
                        the_row();

                        $imagem = get_sub_field('imagem');
                        $data_art = get_sub_field('data_art');
                        $thumb 	= get_sub_field('thumb');
                        $thumb 	= isset($thumb['url']) && $thumb['url'] ? $thumb['url'] : $imagem['url'];

                        if(get_sub_field('link') && !get_sub_field('texto_btn')){
                            $html .= '<a href="'.get_sub_field('link').'" target="'.get_sub_field('nova_aba').'" class="block relative item c_fff">';
                        }

                        $html .= '<div class="slider-container" style="background-image: url(' . $imagem['url'] . ')">';

                        if(get_sub_field('data_art')){
                            $html .= '<div class="data-art row" style="background-image: url(' . $data_art['url'] . ')">';
                            $html .= "<div class='container  row'>";
                        }else{
                            $html .= "<div class='container'>";
                        }

                        if(get_sub_field('titulo')){
                            $html .= '<span class="block title_slide bounceInDown">'.get_sub_field('titulo').'</span>';
                        }

                        if(get_sub_field('texto')){
                            $html .= '<span class="block text_slide bounceInDown">'.get_sub_field('texto').'</span>';
                        }

                        if(get_sub_field('texto_btn')){
                            if(get_sub_field('link')) {
                                $html .= '<a href="'.get_sub_field('link').'" target="'.get_sub_field('nova_aba').'" class="relative item c_fff">';
                            }

                            $html .= '<br/><span class="f12 c_fff btn_' . get_sub_field('cor_btn') . ' c_fff_hover btn_slide bounceInUp" >'.get_sub_field('texto_btn').'&nbsp&nbsp></span>';

                            if(get_sub_field('link')) {
                                $html .= '</a>';
                            }
                        }

                        $html .= '</div>';
                        $html .= '</div>';

	                    if(get_sub_field('data_art')){
		                    $html .= '</div>';
                        }

                        if(get_sub_field('link') && !get_sub_field('texto_btn')){
                            $html .= '</a>';
                        }
                    }

                    echo $html;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}?>

<div class="container">
    <div>

        <!-- Nav tabs -->
        <ul id="tabs-solution" class="tabs-solution nav nav-tabs" role="tablist">
            <li><a class="cursor-pointer" href="/academia/" target="_blank">Academia Serasa Experian</a></li>
            <?php  $tipos_solucao = Serasa_Vitrine_Da_Public::get_information_of_public_taxonomy('tipo-de-solucao-da');
            foreach ($tipos_solucao as $tipo_solucao):?>
                <li role="presentation" class="<?php echo ($count_active === 0) ? 'active' : ''?>"><a data-target="#<?php  echo $tipo_solucao['slugTypeSolution']?>" aria-controls="<?php  echo $tipo_solucao['slugTypeSolution']?>" role="tab" data-toggle="tab"><?php  echo $tipo_solucao['name']?></a></li>
            <?php $count_active++; endforeach;?>
        </ul>

        <!--  Select   -->
        <div class="sub-menu-xs" >
            <select id="sub-menu-xs" class="form-control">
                <option value="Selecione">Selecione uma solução</option>
                <?php  $tipos_solucao = Serasa_Vitrine_Da_Public::get_information_of_public_taxonomy('tipo-de-solucao-da');
                foreach ($tipos_solucao as $tipo_solucao):?>
                    <option value="<?php echo $count_options; ?>"><?php  echo $tipo_solucao['name']?></option>
                <?php $count_options++; endforeach; ?>
            </select>
        </div>


        <!-- Tab panes -->
        <div class="tab-content">
            <?php  $tipos_solucao = Serasa_Vitrine_Da_Public::get_information_of_public_taxonomy('tipo-de-solucao-da');
            foreach ($tipos_solucao as $tipo_solucao):?>
                <div role="tabpanel" class="tab-pane fade <?php echo ($count === 0) ? 'in active' : ''?>" id="<?php  echo $tipo_solucao['slugTypeSolution']?>">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <p class="tab_pane__text"><?php  echo $tipo_solucao['description'];?></p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 align_c_video">
	                        <?php echo $tipo_solucao['video'];?>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <?php $count = 1; while ($count <= 4): ?>
                                <?php  if($tipo_solucao['solucao_' .$count]->post_title !== ' ' && $tipo_solucao['solucao_' .$count]->post_title !== null): ?>
                                    <a href="<?php echo get_permalink($tipo_solucao['solucao_' .$count]->ID)?>">
                                        <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6 tab_width">
                                            <div class="tab_images">
                                                <img src="<?php echo get_field('icone_solucao', $tipo_solucao['solucao_' .$count]->ID);?>" alt="">
                                                <div class="tab_block_paragraph">
                                                    <div class="tab_block_paragraph--text">
                                                        <p><?php echo $tipo_solucao['solucao_' .$count]->post_title?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                <?php endif; ?>
                            <?php $count++; endwhile; ?>
                        </div>
                    </div>
                </div>
            <?php  $count++; endforeach;?>
        </div> <!-- End of Tab panes -->
    </div>
</div>
<?php //if( ! function_exists( 'fetch_feed' ) ) {
//    include_once( ABSPATH . WPINC . '/feed.php' );
//}
//
//$feed = fetch_feed( 'http://blog.serasaexperian.com.br/rss.xml' );
//
//$limit = $feed->get_item_quantity( 3 );
//$items = $feed->get_items( 0, $limit );
//
//if( $limit > 0) {
//    ?>
<!--    <div class="col-xs-12 padding_b_60 padding_t_50" id="blog-home">-->
<!--        <div class="container">-->
<!--            <div class="margin_b_10 relative align_c">-->
<!--                <p style="font-size: 28px;" class="title-box-blog-home">Últimas notícias no blog</p>-->
<!--            </div>-->
<!--            <div class="container">-->
<!--                --><?php
//                foreach ( $items as $item ) {
//                    $image_match = array();
//                    $match = preg_match_all( "#<img\b[^/<>]\bsrc\s*=\s*(\"(?:[^\"]*)\"|'(?:[^']*)'|(?:[^'\">\s]+))#", $item->get_description(), $image_match );
//                    if( $match && is_array( $image_match[1] ) && isset( $image_match[1][0] ) ) {
//                        $image = trim($image_match[1][0], '"\'');
//                    } else {
//                        $image = false;
//                    }
//                    ?>
<!--                    <div class="col-xs-12 col-sm-6 col-md-4 align_center">-->
<!--                        <a class="opacity separator box-blog-home-plugin" href="--><?php //echo $item->get_permalink(); ?><!--">-->
<!--                            <div class="bg_fff col-xs-12 box-blog-home no-padding-left no-padding-right">-->
<!--                                <div class="img-container-blog-home"-->
<!--                                     style="background-position: right;--><?php //echo false !== $image ? "background-image: url({$image});" : ''; ?><!--">-->
<!--                                </div>-->
<!--                                <div class="col-xs-12 col-sm-12 padding-lef-right">-->
<!--                                    <div class="text-left title-blog-home">-->
<!--                                        <div>--><?php //echo $item->get_title(); ?><!--</div>-->
<!--                                    </div>-->
<!--                                    <p class="text-left margin_r_30 margin_b_20">Saiba mais</p>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    --><?php
//                }
//                ?>
<!--                <br style="clear: both;">-->
<!--                <div class="row">-->
<!--                    <div class="col-md-12 align_c">-->
<!--                        <a class="btn btn-blog-home" href="http://blog.serasaexperian.com.br/">Ver todas as notícias</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    --><?php
//}
include "footer-interna.php";?>

